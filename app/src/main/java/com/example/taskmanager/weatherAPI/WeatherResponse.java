package com.example.taskmanager.weatherAPI;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherResponse {

    @SerializedName("main")
    private Main main;

    @SerializedName("weather")
    private List<Weather> weather;

    public Main getMain() {
        return main;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public static class Main {
        @SerializedName("temp")
        private float temperature;

        public float getTemperature() {
            return temperature;
        }
    }

    public static class Weather {
        @SerializedName("main")
        private String mainCondition;

        public String getMainCondition() {
            return mainCondition;
        }
    }
}
