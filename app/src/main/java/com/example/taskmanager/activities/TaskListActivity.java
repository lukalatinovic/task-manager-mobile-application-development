package com.example.taskmanager.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.taskmanager.R;
import com.example.taskmanager.adapters.ViewPagerAdapter;

public class TaskListActivity extends AppCompatActivity {
    public static final String EXTRA_IS_OUTDOOR_TASK = "isOutdoorTask";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);

        ViewPager viewPager = findViewById(R.id.viewPager);

        // Create an instance of the ViewPagerAdapter and set it to the ViewPager
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        boolean isOutdoorTask = getIntent().getBooleanExtra(EXTRA_IS_OUTDOOR_TASK, false);

        viewPager.setCurrentItem(isOutdoorTask ? 1 : 0);
    }
}