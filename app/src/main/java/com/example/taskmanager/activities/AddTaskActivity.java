package com.example.taskmanager.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.taskmanager.R;
import com.example.taskmanager.database.TaskDbHelper;
import com.example.taskmanager.models.Task;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AddTaskActivity extends AppCompatActivity {
    private EditText editTextTaskName;
    private TextView textViewDueDate;
    private Spinner spinnerPriority;
    private Spinner spinnerStatus;
    private Spinner spinnerType;
    private EditText editTextDetails;
    private Task taskToEdit;
    private TaskDbHelper dbHelper;
    public static final int ADD_TASK_REQUEST_CODE = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        editTextTaskName = findViewById(R.id.editTextTaskName);
        textViewDueDate = findViewById(R.id.editTextDueDate);
        spinnerPriority = findViewById(R.id.spinnerPriority);
        spinnerStatus = findViewById(R.id.spinnerStatus);
        spinnerType = findViewById(R.id.spinnerType);
        editTextDetails = findViewById(R.id.editTextDetails);
        Button buttonSaveTask = findViewById(R.id.buttonSaveTask);
        Button buttonPickDate = findViewById(R.id.buttonPickDate);

        dbHelper = new TaskDbHelper(this);

        ArrayAdapter<CharSequence> typeAdapter = ArrayAdapter.createFromResource(
                this,
                R.array.type_options,
                android.R.layout.simple_spinner_item
        );
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType.setAdapter(typeAdapter);

        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // No changes here
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // Do nothing here
            }
        });

        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // No changesMade here
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // Do nothing here
            }
        });

        // Call the addTask method to set default values
        Intent intent = getIntent();
        if (intent != null) {
            boolean isNewTask = intent.getBooleanExtra("isNewTask", true);
            String defaultType = intent.getStringExtra("defaultType");

            if (isNewTask) {
                addTask(defaultType); // Pass the default type value
            } else {
                taskToEdit = (Task) intent.getSerializableExtra("taskToEdit");
                if (taskToEdit != null) {
                    Log.d("AddTaskActivity", "Editing task: " + taskToEdit);
                    editTask(taskToEdit);
                }
            }
        }

        buttonSaveTask.setOnClickListener(view -> {
            saveTask();
        });

        buttonPickDate.setOnClickListener(view -> {
            showDatePickerDialog();
        });

        editTextTaskName.addTextChangedListener(new SimpleTextWatcher());

        editTextDetails.addTextChangedListener(new SimpleTextWatcher());
    }

    private class SimpleTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // Text has been changed, no changesMade handling here
        }
    }

    private void addTask(String defaultType) {
        taskToEdit = null;

        int defaultStatusPosition = getStatusPosition("In progress");
        spinnerStatus.setSelection(defaultStatusPosition);

        int typePosition = getTypePosition(defaultType);
        spinnerType.setSelection(typePosition);

        spinnerType.setEnabled(false);
    }
    
    private void editTask(Task taskToEdit) {
        editTextTaskName.setText(taskToEdit.getTaskName());
        textViewDueDate.setText(taskToEdit.getDueDate());
        int priorityPosition = getPriorityPosition(taskToEdit.getPriority());
        spinnerPriority.setSelection(priorityPosition);

        if (taskToEdit.getStatus() != null) {
            findViewById(R.id.textViewStatusLabel).setVisibility(View.VISIBLE);
            spinnerStatus.setVisibility(View.VISIBLE);
            int statusPosition = getStatusPosition(taskToEdit.getStatus());
            spinnerStatus.setSelection(statusPosition);
        }

        int typePosition = getTypePosition(taskToEdit.getType());
        spinnerType.setSelection(typePosition);

        editTextDetails.setText(taskToEdit.getDetails());
    }

    private void saveTask() {
        String taskName = editTextTaskName.getText().toString().trim();
        String dueDate = textViewDueDate.getText().toString().trim();
        String priority = spinnerPriority.getSelectedItem().toString();
        String details = editTextDetails.getText().toString().trim();
        String status = spinnerStatus.getSelectedItem().toString();
        String type = spinnerType.getSelectedItem().toString();

        if (taskToEdit == null) {
            status = getStatusBasedOnDueDate(dueDate);
        }

        if (taskToEdit != null && !changesMade()) {
            Toast.makeText(this, "No changes made!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (taskName.isEmpty() || dueDate.isEmpty() || priority.isEmpty()) {
            editTextTaskName.setError("Required");
            textViewDueDate.setError("Required");
            return;
        }

        if (taskToEdit == null) {
            dbHelper.insertTask(taskName, dueDate, priority, details, status, type);
            Toast.makeText(this, "New task added successfully", Toast.LENGTH_SHORT).show();
        } else {
            Task updatedTask = new Task(
                    taskToEdit.getTaskId(),
                    taskName,
                    dueDate,
                    priority,
                    details,
                    status,
                    type
            );
            dbHelper.updateTask(updatedTask);
            Toast.makeText(this, "Task updated successfully", Toast.LENGTH_SHORT).show();
            sendUpdatedTaskData(updatedTask);
        }

        // Set the result with the ADD_TASK_REQUEST_CODE
        Intent resultIntent = new Intent();
        setResult(Activity.RESULT_OK, resultIntent);

        finish();
    }


    private String getStatusBasedOnDueDate(String dueDate) {
        try {
            Date dueDateObj = TaskDbHelper.DATE_FORMAT.parse(dueDate);

            Date currentDate = new Date();

            if (dueDateObj.before(currentDate)) {
                return "Expired";
            } else {
                return "In progress";
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return "In progress";
        }
    }

    private void sendUpdatedTaskData(Task updatedTask) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("updatedTask", updatedTask);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    private void showDiscardChangesDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Discard Changes");
        builder.setMessage("Are you sure you want to discard the changes?");
        builder.setPositiveButton("Discard", (dialog, which) -> {
            finish();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> {
            dialog.dismiss();
        });
        builder.show();
    }

    private void showDatePickerDialog() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(
                this,
                (datePicker, year1, month1, day1) -> {
                    String formattedDate = String.format(Locale.getDefault(), "%04d-%02d-%02d", year1, month1 + 1, day1);
                    textViewDueDate.setText(formattedDate);
                },
                year, month, day);

        datePickerDialog.show();
    }

    @Override
    public void onBackPressed() {
        if (taskToEdit == null) {
            if (!editTextTaskName.getText().toString().trim().isEmpty()
                    || !textViewDueDate.getText().toString().trim().isEmpty()
                    || !spinnerPriority.getSelectedItem().toString().isEmpty()
                    || !editTextDetails.getText().toString().trim().isEmpty()
                    || !spinnerStatus.getSelectedItem().toString().isEmpty()) {
                showDiscardChangesDialog();
            } else {
                super.onBackPressed();
            }
        } else {
            if (changesMade()) {
                showDiscardChangesDialog();
            } else {
                super.onBackPressed();
            }
        }
    }

    private boolean changesMade() {
        if (taskToEdit == null) {
            return !editTextTaskName.getText().toString().trim().isEmpty()
                    || !textViewDueDate.getText().toString().trim().isEmpty()
                    || !spinnerPriority.getSelectedItem().toString().isEmpty()
                    || !editTextDetails.getText().toString().trim().isEmpty()
                    || !spinnerStatus.getSelectedItem().toString().isEmpty();
        } else {
            return !(taskToEdit.getTaskName() != null && taskToEdit.getTaskName().equals(editTextTaskName.getText().toString().trim()))
                    || !(taskToEdit.getDueDate() != null && taskToEdit.getDueDate().equals(textViewDueDate.getText().toString().trim()))
                    || !(taskToEdit.getPriority() != null && taskToEdit.getPriority().equals(spinnerPriority.getSelectedItem().toString()))
                    || !(taskToEdit.getStatus() != null && taskToEdit.getStatus().equals(spinnerStatus.getSelectedItem().toString()))
                    || !(taskToEdit.getDetails() != null && taskToEdit.getDetails().equals(editTextDetails.getText().toString().trim()));
        }
    }

    private int getPriorityPosition(String priority) {
        String[] priorities = getResources().getStringArray(R.array.priority_options);
        for (int i = 0; i < priorities.length; i++) {
            if (priorities[i].equalsIgnoreCase(priority)) {
                return i;
            }
        }
        return 0;
    }

    private int getStatusPosition(String status) {
        String[] statusOptions = getResources().getStringArray(R.array.status_options);
        for (int i = 0; i < statusOptions.length; i++) {
            if (statusOptions[i].equalsIgnoreCase(status)) {
                return i;
            }
        }
        return 0;
    }

    private int getTypePosition(String type) {
        String[] types = getResources().getStringArray(R.array.type_options);
        for (int i = 0; i < types.length; i++) {
            if (types[i].equalsIgnoreCase(type)) {
                return i;
            }
        }
        return 0;
    }
}