package com.example.taskmanager.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.taskmanager.R;
import com.example.taskmanager.models.Task;
import com.example.taskmanager.interfaces.WeatherApiService;
import com.example.taskmanager.weatherAPI.RetrofitClient;
import com.example.taskmanager.weatherAPI.WeatherResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskDetailsActivity extends AppCompatActivity {
    private static final int EDIT_TASK_REQUEST_CODE = 1002;
    private static final String OPEN_WEATHER_MAP_API_KEY = "6efe66b24bfaa45012c44243638aeae0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("task")) {
            Task task = (Task) intent.getSerializableExtra("task");

            TextView textViewTaskName = findViewById(R.id.textViewTaskName);
            TextView textViewDueDate = findViewById(R.id.textViewDueDate);
            TextView textViewPriority = findViewById(R.id.textViewPriority);
            TextView textViewStatus = findViewById(R.id.textViewStatus);
            TextView textViewDetails = findViewById(R.id.textViewDetails);
            TextView textViewType = findViewById(R.id.textViewType);
            TextView weatherLabel = findViewById(R.id.textViewWeatherLabel);

            if (task != null) {
                Log.d("TaskDetailsActivity", "Task: " + task);
                textViewTaskName.setText(task.getTaskName());
                textViewDueDate.setText(task.getDueDate());
                textViewPriority.setText(task.getPriority());
                textViewStatus.setText(task.getStatus());
                textViewDetails.setText(task.getDetails());
                textViewType.setText(task.getType());

                if ("outdoor".equalsIgnoreCase(task.getType())) {
                    getWeatherInformation();
                } else {
                    weatherLabel.setVisibility(View.GONE);
                }
            }

            Button buttonEditTask = findViewById(R.id.buttonEditTask);
            buttonEditTask.setOnClickListener(v -> {
                // Pass the task details to the AddTaskActivity
                editTask(task);
            });
        } else {
            Toast.makeText(this, "Task details not available", Toast.LENGTH_SHORT).show();
        }
    }

    private void editTask(Task task) {
        Intent editIntent = new Intent(this, AddTaskActivity.class);
        editIntent.putExtra("isNewTask", false);
        editIntent.putExtra("taskToEdit", task);
        startActivityForResult(editIntent, EDIT_TASK_REQUEST_CODE);
    }

    private void getWeatherInformation() {
        WeatherApiService weatherApiService = RetrofitClient.getClient().create(WeatherApiService.class);

        String city = "Novi Sad";
        Call<WeatherResponse> call = weatherApiService.getWeather(city, OPEN_WEATHER_MAP_API_KEY, "metric");

        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    WeatherResponse weatherResponse = response.body();
                    handleWeatherResponse(weatherResponse);
                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                Log.e("Weather API", "Error fetching weather data: " + t.getMessage());
            }
        });
    }

    private void handleWeatherResponse(WeatherResponse weatherResponse) {
        if (weatherResponse != null && weatherResponse.getMain() != null) {
            double temperature = weatherResponse.getMain().getTemperature();

            String formattedTemperature = String.valueOf((int) temperature);

            TextView textViewTemperature = findViewById(R.id.textViewTemperature);
            textViewTemperature.setText(String.format("%s°C", formattedTemperature));

            if (weatherResponse.getWeather() != null && !weatherResponse.getWeather().isEmpty()) {
                WeatherResponse.Weather weather = weatherResponse.getWeather().get(0);
                String mainCondition = weather.getMainCondition();

                int iconResId = R.drawable.ic_default;

                switch (mainCondition) {
                    case "Clear":
                        iconResId = R.drawable.ic_clear_sky;
                        break;
                    case "Clouds":
                        iconResId = R.drawable.ic_cloudy;
                        break;
                    case "Rain":
                        iconResId = R.drawable.ic_rainy;
                        break;
                    case "Snow":
                        iconResId = R.drawable.ic_snowy;
                        break;
                    case "Mist":
                        iconResId = R.drawable.ic_mist;
                        break;

                    default:
                        // Use the default icon
                        break;
                }

                ImageView imageViewWeatherIcon = findViewById(R.id.imageViewWeatherIcon);
                imageViewWeatherIcon.setImageResource(iconResId);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == EDIT_TASK_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            Task updatedTask = (Task) data.getSerializableExtra("updatedTask");

            Intent resultIntent = new Intent();
            resultIntent.putExtra("updatedTask", updatedTask);
            setResult(RESULT_OK, resultIntent);

            finish();
        }
    }

//    private void updateUITaskDetails(Task updatedTask) {
//        TextView textViewTaskName = findViewById(R.id.textViewTaskName);
//        TextView textViewDueDate = findViewById(R.id.textViewDueDate);
//        TextView textViewPriority = findViewById(R.id.textViewPriority);
//        TextView textViewStatus = findViewById(R.id.textViewStatus);
//        TextView textViewDetails = findViewById(R.id.textViewDetails);
//
//        if (updatedTask != null) {
//            textViewTaskName.setText(updatedTask.getTaskName());
//            textViewDueDate.setText(updatedTask.getDueDate());
//            textViewPriority.setText(updatedTask.getPriority());
//            textViewStatus.setText(updatedTask.getStatus());
//            textViewDetails.setText(updatedTask.getDetails());
//        }
//    }
}