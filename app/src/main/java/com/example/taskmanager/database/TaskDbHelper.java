package com.example.taskmanager.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.taskmanager.models.Task;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class TaskDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "task_manager.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "tasks";
    public static final String COLUMN_TASK_ID = "task_id";
    public static final String COLUMN_TASK_NAME = "task_name";
    public static final String COLUMN_DUE_DATE = "due_date";
    public static final String COLUMN_PRIORITY = "priority";
    public static final String COLUMN_DETAILS = "details";
    public static final String COLUMN_PRIORITY_LOW = "Low";
    public static final String COLUMN_PRIORITY_MEDIUM = "Medium";
    public static final String COLUMN_PRIORITY_HIGH = "High";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_STATUS_COMPLETED = "Completed";
    public static final String COLUMN_STATUS_IN_PROGRESS = "In progress";
    public static final String COLUMN_STATUS_EXPIRED = "Expired";
    public static final String COLUMN_TYPE = "type";
    public static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd";
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_PATTERN);
    public TaskDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableQuery = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_TASK_ID + " TEXT PRIMARY KEY," +
                COLUMN_TASK_NAME + " TEXT," +
                COLUMN_DUE_DATE + " TEXT," +
                COLUMN_PRIORITY + " TEXT," +
                COLUMN_DETAILS + " TEXT," +
                COLUMN_STATUS + " TEXT DEFAULT '" + COLUMN_STATUS_IN_PROGRESS + "'," +
                COLUMN_TYPE + " TEXT)";

        db.execSQL(createTableQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public String insertTask(String taskName, String dueDate, String priority, String details, String status, String type) {
        SQLiteDatabase db = getWritableDatabase();
        String taskId = generateTaskId();

        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_TASK_ID, taskId);
            values.put(COLUMN_TASK_NAME, taskName);
            values.put(COLUMN_DUE_DATE, dueDate);
            values.put(COLUMN_PRIORITY, priority);
            values.put(COLUMN_DETAILS, details);
            values.put(COLUMN_STATUS, status);
            values.put(COLUMN_TYPE, type);

            db.insert(TABLE_NAME, null, values);
        } catch (Exception e) {
            Log.e("TaskDbHelper", "Error inserting task: " + e.getMessage());
            taskId = null;
        } finally {
            db.close();
        }

        return taskId;
    }

    public void updateTask(Task task) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TASK_NAME, task.getTaskName());
        values.put(COLUMN_DUE_DATE, task.getDueDate());
        values.put(COLUMN_PRIORITY, task.getPriority());
        values.put(COLUMN_DETAILS, task.getDetails());
        values.put(COLUMN_STATUS, task.getStatus());
        values.put(COLUMN_TYPE, task.getType());

        db.update(TABLE_NAME, values, COLUMN_TASK_ID + " = ?", new String[]{task.getTaskId()});
        db.close();
    }

    public List<Task> getAllTasks() {
        List<Task> taskList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                Task task = Task.fromCursor(cursor);
                taskList.add(task);
            } while (cursor.moveToNext());

            cursor.close();
        }

        db.close();
        return taskList;
    }

//    public Task getTask(String taskId) {
//        SQLiteDatabase db = getReadableDatabase();
//
//        String selection = COLUMN_TASK_ID + " = ?";
//        String[] selectionArgs = new String[]{taskId};
//
//        Cursor cursor = db.query(TABLE_NAME, null, selection, selectionArgs, null, null, null);
//
//        Task task = null;
//
//        if (cursor != null && cursor.moveToFirst()) {
//            task = Task.fromCursor(cursor);
//            cursor.close();
//        }
//
//        db.close();
//
//        return task;
//    }

    public List<Task> getTasksByType(String type) {
        List<Task> taskList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + COLUMN_TYPE + " = ?" +
                " ORDER BY " + COLUMN_DUE_DATE + " ASC";

        Cursor cursor = db.rawQuery(query, new String[]{type});

        if (cursor.moveToFirst()) {
            do {
                Task task = createTaskFromCursor(cursor);
                taskList.add(task);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return taskList;
    }

    public List<Task> searchTasks(String searchQuery, String type) {
        List<Task> taskList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        String selection = "(" + COLUMN_TASK_NAME + " LIKE ? OR " + COLUMN_DETAILS + " LIKE ?) AND " + COLUMN_TYPE + " = ?";
        String[] selectionArgs = new String[]{"%" + searchQuery + "%", "%" + searchQuery + "%", type};

        Cursor cursor = db.query(TABLE_NAME, null, selection, selectionArgs, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            do {
                Task task = Task.fromCursor(cursor);
                taskList.add(task);
            } while (cursor.moveToNext());

            cursor.close();
        }

        db.close();
        return taskList;
    }

    public List<Task> getTasksSortedByDueDateAscending(String type) {
        return getTasksSortedByDueDate(type, "ASC");
    }

    public List<Task> getTasksSortedByDueDateDescending(String type) {
        return getTasksSortedByDueDate(type, "DESC");
    }

    private List<Task> getTasksSortedByDueDate(String type, String sortOrder) {
        List<Task> taskList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + COLUMN_TYPE + " = ?" +
                " ORDER BY " + COLUMN_DUE_DATE + " " + sortOrder;

        Cursor cursor = db.rawQuery(query, new String[]{type});

        if (cursor.moveToFirst()) {
            do {
                Task task = createTaskFromCursor(cursor);
                taskList.add(task);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return taskList;
    }

    public List<Task> getTasksSortedByPriorityLow(String type) {
        return getTasksSortedByPriority(type, COLUMN_PRIORITY_LOW);
    }

    public List<Task> getTasksSortedByPriorityMedium(String type) {
        return getTasksSortedByPriority(type, COLUMN_PRIORITY_MEDIUM);
    }

    public List<Task> getTasksSortedByPriorityHigh(String type) {
        return getTasksSortedByPriority(type, COLUMN_PRIORITY_HIGH);
    }

    private List<Task> getTasksSortedByPriority(String type, String priority) {
        List<Task> taskList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + COLUMN_TYPE + " = ? AND " + COLUMN_PRIORITY + " = ?" +
                " ORDER BY " + COLUMN_DUE_DATE + " ASC";

        Cursor cursor = db.rawQuery(query, new String[]{type, priority});

        if (cursor.moveToFirst()) {
            do {
                Task task = createTaskFromCursor(cursor);
                taskList.add(task);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return taskList;
    }

    public List<Task> getTasksSortedByStatusCompleted(String type) {
        return getTasksSortedByStatus(type, COLUMN_STATUS_COMPLETED);
    }

    public List<Task> getTasksSortedByStatusInProgress(String type) {
        return getTasksSortedByStatus(type, COLUMN_STATUS_IN_PROGRESS);
    }

    public List<Task> getTasksSortedByStatusExpired(String type) {
        return getTasksSortedByStatus(type, COLUMN_STATUS_EXPIRED);
    }

    private List<Task> getTasksSortedByStatus(String type, String status) {
        List<Task> taskList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        String query = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + COLUMN_TYPE + " = ? AND " + COLUMN_STATUS + " = ?" +
                " ORDER BY " + COLUMN_DUE_DATE + " ASC";

        Cursor cursor = db.rawQuery(query, new String[]{type, status});

        if (cursor.moveToFirst()) {
            do {
                Task task = createTaskFromCursor(cursor);
                taskList.add(task);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return taskList;
    }

    private Task createTaskFromCursor(Cursor cursor) {
        return Task.fromCursor(cursor);
    }

    private String generateTaskId() {
        return java.util.UUID.randomUUID().toString();
    }

    public void deleteTask(String taskId) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_NAME, COLUMN_TASK_ID + " = ?", new String[]{taskId});
        db.close();
    }
}