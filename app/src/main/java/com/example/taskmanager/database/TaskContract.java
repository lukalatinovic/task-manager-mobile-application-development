package com.example.taskmanager.database;

import android.provider.BaseColumns;

public class TaskContract {

    private TaskContract() {
    }

    public static class TaskEntry implements BaseColumns {
        public static final String TABLE_NAME = "tasks";
        public static final String COLUMN_TASK_NAME = "task_name";
        public static final String COLUMN_DUE_DATE = "due_date";
        public static final String COLUMN_PRIORITY = "priority";
        public static final String COLUMN_DETAILS = "details";
        public static final String COLUMN_STATUS = "status";

        public static final String STATUS_COMPLETED = "Completed";
        public static final String STATUS_IN_PROGRESS = "In Progress";
        public static final String STATUS_EXPIRED = "Expired";
    }
}