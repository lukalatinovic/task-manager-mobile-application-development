package com.example.taskmanager.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.provider.BaseColumns;

import com.example.taskmanager.database.TaskDbHelper;

import java.io.Serializable;

public class Task implements Serializable {
    private String taskId;
    private String taskName;
    private String dueDate;
    private String priority;
    private String details;
    private String status;
    private String type;
    public static final String COLUMN_TASK_NAME = "task_name";
    public static final String COLUMN_DUE_DATE = "due_date";
    public static final String COLUMN_PRIORITY = "priority";
    public static final String COLUMN_DETAILS = "details";
    public static final String COLUMN_STATUS = "status";

    public Task() {
    }

    public Task(String taskId, String taskName, String dueDate, String priority, String details, String status, String type) {
        this.taskId = taskId;
        this.taskName = taskName;
        this.dueDate = dueDate;
        this.priority = priority;
        this.details = details;
        this.status = status;
        this.type = type;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    // Create a Task instance from a Cursor
    public static Task fromCursor(Cursor cursor) {
        String taskId = cursor.getString(cursor.getColumnIndexOrThrow(TaskDbHelper.COLUMN_TASK_ID));
        String taskName = cursor.getString(cursor.getColumnIndexOrThrow(TaskDbHelper.COLUMN_TASK_NAME));
        String dueDate = cursor.getString(cursor.getColumnIndexOrThrow(TaskDbHelper.COLUMN_DUE_DATE));
        String priority = cursor.getString(cursor.getColumnIndexOrThrow(TaskDbHelper.COLUMN_PRIORITY));
        String details = cursor.getString(cursor.getColumnIndexOrThrow(TaskDbHelper.COLUMN_DETAILS));
        String status = cursor.getString(cursor.getColumnIndexOrThrow(TaskDbHelper.COLUMN_STATUS));
        String type = cursor.getString(cursor.getColumnIndexOrThrow(TaskDbHelper.COLUMN_TYPE));

        return new Task(taskId, taskName, dueDate, priority, details, status, type);
    }
}