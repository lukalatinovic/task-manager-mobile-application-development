package com.example.taskmanager.interfaces;

import android.view.View;

public interface TaskListCallback {
    void showFilterMenu(View v);
    void handleDueDateFilter(boolean isFirstToExpire);
    void clearFilters();
    void handlePriorityFilter(int priority);
    void handleStatusFilter(int status);
    void onAddTaskClick();
}