package com.example.taskmanager.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.taskmanager.R;
import com.example.taskmanager.database.TaskDbHelper;
import com.example.taskmanager.models.Task;

import java.util.List;

public class IndoorTaskFragment extends TaskListFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_task_list, container, false);

        Button addTaskButton = rootView.findViewById(R.id.buttonAddTask);
        addTaskButton.setOnClickListener(v -> onAddTaskClick());

        TextView fragmentTitleTextView = rootView.findViewById(R.id.textViewFragmentTitle);

        fragmentTitleTextView.setText("INDOOR TASKS");

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        dbHelper = new TaskDbHelper(requireContext().getApplicationContext());

        List<Task> taskList = dbHelper.getTasksByType(getFragmentType());
        taskAdapter.updateTaskList(taskList);
        updateEmptyState();
    }

    @Override
    public void showFilterMenu(View v) {
        super.showFilterMenu(v);
    }

    @Override
    public void handleDueDateFilter(boolean isFirstToExpire) {
        super.handleDueDateFilter(isFirstToExpire);
    }

    @Override
    public void clearFilters() {
        super.clearFilters();
    }

    @Override
    public void handlePriorityFilter(int priority) {
        super.handlePriorityFilter(priority);
    }

    @Override
    public void handleStatusFilter(int status) {
        super.handleStatusFilter(status);
    }

    @Override
    public void onAddTaskClick() {
        Log.d("IndoorTaskFragment", "Add Task Clicked");

        Task newTask = new Task();
        newTask.setType("Indoor");

        startAddTaskActivity(newTask, true, "Indoor");
    }

    @Override
    protected String getFragmentType() {
        return "Indoor";
    }
}