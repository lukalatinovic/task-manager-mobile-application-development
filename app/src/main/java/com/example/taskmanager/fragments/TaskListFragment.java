package com.example.taskmanager.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taskmanager.R;
import com.example.taskmanager.activities.AddTaskActivity;
import com.example.taskmanager.activities.TaskDetailsActivity;
import com.example.taskmanager.adapters.TaskAdapter;
import com.example.taskmanager.database.TaskDbHelper;
import com.example.taskmanager.interfaces.TaskListCallback;
import com.example.taskmanager.models.Task;

import java.util.List;

public abstract class TaskListFragment extends Fragment implements TaskAdapter.OnTaskClickListener, TaskAdapter.OnTaskLongClickListener, TaskListCallback {
    public static final int ADD_TASK_REQUEST_CODE = 1001;
    private static final int EDIT_TASK_REQUEST_CODE = 1002;
    private static final String FILTER_DUE_DATE = "due_date";
    private RecyclerView recyclerView;
    protected TaskAdapter taskAdapter;
    protected TaskDbHelper dbHelper;
    private View emptyStateTextView;
    private boolean isSearchActive = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new TaskDbHelper(requireContext().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task_list, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(layoutManager);

        List<Task> taskList = dbHelper.getAllTasks();
        taskAdapter = new TaskAdapter(requireContext(), taskList, this);
        recyclerView.setAdapter(taskAdapter);

        taskAdapter.setOnTaskLongClickListener(this);

        emptyStateTextView = view.findViewById(R.id.emptyStateTextView);

        updateEmptyState();

        SearchView searchView = view.findViewById(R.id.searchView);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                updateTaskListAndEmptyState(newText, TextUtils.isEmpty(newText) ? "" : FILTER_DUE_DATE, getFragmentType());
                return true;
            }
        });

        searchView.setOnQueryTextFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus && TextUtils.isEmpty(searchView.getQuery())) {
                searchView.setIconified(true);
            }
        });

        ImageView filterIcon = view.findViewById(R.id.filterIcon);

        filterIcon.setOnClickListener(v -> showFilterMenu(v));
    }

    private void updateTaskListAndEmptyState(String searchQuery, String filter, String type) {
        isSearchActive = !TextUtils.isEmpty(searchQuery);

        List<Task> updatedTaskList;

        if (!isSearchActive) {
            updatedTaskList = dbHelper.getTasksByType(type);
        } else {
            updatedTaskList = dbHelper.searchTasks(searchQuery, type);
        }

        taskAdapter.updateTaskList(updatedTaskList);
        updateEmptyState();
    }

    protected void updateEmptyState() {
        if (taskAdapter.getItemCount() == 0 && !isSearchActive) {
            emptyStateTextView.setVisibility(View.VISIBLE);
            Toast.makeText(requireContext(), "Task list is empty", Toast.LENGTH_SHORT).show();
        } else {
            emptyStateTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dbHelper.close();
    }

    @Override
    public void showFilterMenu(View v) {
        PopupMenu popupMenu = new PopupMenu(requireContext(), v);
        popupMenu.getMenuInflater().inflate(R.menu.filter_menu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.filterOption1) {
                return true;
            } else if (item.getItemId() == R.id.filterOption3) {
                return true;
            } else if (item.getItemId() == R.id.dueDateOption1) {
                handleDueDateFilter(true);
                return true;
            } else if (item.getItemId() == R.id.dueDateOption2) {
                handleDueDateFilter(false);
                return true;
            } else if (item.getItemId() == R.id.priorityOption1 ||
                    item.getItemId() == R.id.priorityOption2 ||
                    item.getItemId() == R.id.priorityOption3) {
                handlePriorityFilter(item.getItemId());
                return true;
            } else if (item.getItemId() == R.id.statusOption1 ||
                    item.getItemId() == R.id.statusOption2 ||
                    item.getItemId() == R.id.statusOption3) {
                handleStatusFilter(item.getItemId());
                return true;
            } else if (item.getItemId() == R.id.clearFiltersOption) {
                clearFilters();
                return true;
            } else {
                return false;
            }
        });

        popupMenu.show();
    }

    @Override
    public void onTaskClick(int position) {
        Task selectedTask = taskAdapter.getItem(position);

        Intent intent = new Intent(requireContext(), TaskDetailsActivity.class);
        intent.putExtra("task", selectedTask);
        startActivityForResult(intent, EDIT_TASK_REQUEST_CODE);
    }

    protected void startAddTaskActivity(Task task, boolean isNewTask, String defaultType) {
        Intent intent = new Intent(getActivity(), AddTaskActivity.class);
        intent.putExtra("isNewTask", isNewTask);
        intent.putExtra("taskToEdit", task);
        intent.putExtra("defaultType", defaultType);
        startActivityForResult(intent, ADD_TASK_REQUEST_CODE);
    }

    @Override
    public void onTaskLongClick(int position) {
        showDeleteConfirmationDialog(position);
    }

    private void showDeleteConfirmationDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setMessage("Do you want to delete this task?")
                .setPositiveButton("Delete", (dialog, which) -> deleteTask(position))
                .setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss())
                .create()
                .show();
    }

    private void deleteTask(int position) {
        if (position >= 0 && position < taskAdapter.getItemCount()) {
            Task taskToDelete = taskAdapter.getItem(position);
            dbHelper.deleteTask(taskToDelete.getTaskId());
            taskAdapter.removeTask(position);
            updateEmptyState();
            Toast.makeText(requireContext(), "Task deleted successfully", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void handleDueDateFilter(boolean isFirstToExpire) {
        List<Task> updatedTaskList;

        if (isFirstToExpire) {
            updatedTaskList = dbHelper.getTasksSortedByDueDateAscending(getFragmentType());
        } else {
            updatedTaskList = dbHelper.getTasksSortedByDueDateDescending(getFragmentType());
        }

        taskAdapter.updateTaskList(updatedTaskList);

        updateEmptyState();
    }

    @Override
    public void clearFilters() {
        List<Task> allTasks = dbHelper.getTasksByType(getFragmentType());
        taskAdapter.updateTaskList(allTasks);
        updateEmptyState();
    }

    @Override
    public void handlePriorityFilter(int priority) {
        List<Task> updatedTaskList;

        if (priority == R.id.priorityOption1) {
            updatedTaskList = dbHelper.getTasksSortedByPriorityLow(getFragmentType());
        } else if (priority == R.id.priorityOption2) {
            updatedTaskList = dbHelper.getTasksSortedByPriorityMedium(getFragmentType());
        } else if (priority == R.id.priorityOption3) {
            updatedTaskList = dbHelper.getTasksSortedByPriorityHigh(getFragmentType());
        } else {
            updatedTaskList = dbHelper.getAllTasks();
        }

        taskAdapter.updateTaskList(updatedTaskList);

        updateEmptyState();
    }

    @Override
    public void handleStatusFilter(int status) {
        List<Task> updatedTaskList;

        if (status == R.id.statusOption1) {
            updatedTaskList = dbHelper.getTasksSortedByStatusCompleted(getFragmentType());
        } else if (status == R.id.statusOption2) {
            updatedTaskList = dbHelper.getTasksSortedByStatusInProgress(getFragmentType());
        } else if (status == R.id.statusOption3) {
            updatedTaskList = dbHelper.getTasksSortedByStatusExpired(getFragmentType());
        } else {
            updatedTaskList = dbHelper.getAllTasks();
        }

        taskAdapter.updateTaskList(updatedTaskList);

        updateEmptyState();
    }

    @Override
    public void onAddTaskClick() {
        // Pass the fragment type along with the new task
        startAddTaskActivity(null, true, getFragmentType());
    }

    protected abstract String getFragmentType();

    protected void refreshTaskList() {
        List<Task> updatedTaskList = dbHelper.getTasksByType(getFragmentType());
        taskAdapter.updateTaskList(updatedTaskList);
        taskAdapter.notifyDataSetChanged(); // Notify the adapter that the data has changed
        updateEmptyState();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == EDIT_TASK_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            Task updatedTask = (Task) data.getSerializableExtra("updatedTask");
            refreshTaskList();
        } else if (requestCode == ADD_TASK_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            boolean isNewTask = data.getBooleanExtra("isNewTask", false);

            if (isNewTask) {
                refreshTaskList();
            } else {
                Task updatedTask = (Task) data.getSerializableExtra("updatedTask");
                refreshTaskList();
            }
        }
    }
}