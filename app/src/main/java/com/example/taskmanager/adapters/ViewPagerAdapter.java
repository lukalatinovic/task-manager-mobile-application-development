package com.example.taskmanager.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.taskmanager.fragments.IndoorTaskFragment;
import com.example.taskmanager.fragments.OutdoorTaskFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(@NonNull FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return (position == 0) ? new IndoorTaskFragment() : new OutdoorTaskFragment();
    }

    @Override
    public int getCount() {
        return 2;
    }
}