package com.example.taskmanager.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taskmanager.R;
import com.example.taskmanager.models.Task;

import java.util.List;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskViewHolder> {
    private List<Task> taskList;
    private OnTaskClickListener onTaskClickListener;
    private OnTaskLongClickListener longClickListener;
    private Context context;

    public TaskAdapter(Context context, List<Task> taskList, OnTaskClickListener onTaskClickListener) {
        this.context = context;
        this.taskList = taskList;
        this.onTaskClickListener = onTaskClickListener;
    }

    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_item_layout, parent, false);
        return new TaskViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        Task currentTask = taskList.get(position);

        holder.textViewTaskName.setText(currentTask.getTaskName());
        holder.textViewDueDate.setText(currentTask.getDueDate());
        holder.textViewPriority.setText(currentTask.getPriority());
        holder.textViewStatus.setText(currentTask.getStatus());

        int statusBackgroundColor = getColorForStatus(currentTask.getStatus());
        holder.textViewStatus.setBackgroundColor(statusBackgroundColor);

        holder.itemView.setOnClickListener(v -> {
            if (onTaskClickListener != null) {
                onTaskClickListener.onTaskClick(position);
            }
        });

        holder.itemView.setOnLongClickListener(v -> {
            if (longClickListener != null) {
                longClickListener.onTaskLongClick(position);
                return true;
            }
            return false;
        });
    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    public Task getItem(int position) {
        return taskList.get(position);
    }

    public void setOnTaskLongClickListener(OnTaskLongClickListener listener) {
        this.longClickListener = listener;
    }

    public interface OnTaskLongClickListener {
        void onTaskLongClick(int position);
    }

    public interface OnTaskClickListener {
        void onTaskClick(int position);
    }

    public class TaskViewHolder extends RecyclerView.ViewHolder {
        TextView textViewTaskName;
        TextView textViewDueDate;
        TextView textViewPriority;
        TextView textViewStatus;

        public TaskViewHolder(View itemView) {
            super(itemView);
            textViewTaskName = itemView.findViewById(R.id.textViewTaskName);
            textViewDueDate = itemView.findViewById(R.id.textViewDueDate);
            textViewPriority = itemView.findViewById(R.id.textViewPriority);
            textViewStatus = itemView.findViewById(R.id.textViewStatus);
        }
    }

    public void updateTaskList(List<Task> newTaskList) {
        taskList.clear();
        taskList.addAll(newTaskList);
        notifyDataSetChanged();
    }

    public void removeTask(int position) {
        if (position >= 0 && position < taskList.size()) {
            taskList.remove(position);
            notifyItemRemoved(position);
        }
    }

    private int getColorForStatus(String status) {
        if (status != null) {
            switch (status.toLowerCase()) {
                case "completed":
                    return Color.parseColor("#8BC34A"); // Light green
                case "expired":
                    return Color.parseColor("#FF7F50"); // Coral
                case "in progress":
                    return Color.parseColor("#FFD700"); // Light orange
                default:
                    return Color.TRANSPARENT;
            }
        } else {
            // No color
            return Color.TRANSPARENT;
        }
    }
}